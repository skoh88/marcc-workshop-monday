#!/bin/bash

a=Hello
b=World

echo $a $b

dir="/home-1/class-2s65@jhu.edu"

ls $dir

# single quotes are literal
c='Hello World'
echo $c

# double quotes substitute variables
d="Big $c"
echo $d

# save contents of a command into variable
e=$(ls $dir | wc -l) # piping directory to word count (wc)
echo $e

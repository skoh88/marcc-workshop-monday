#!/bin/bash -l

#SBATCH
#SBATCH --job-name=MyTutorial
#SBATCH --time=00:10:00
#SBATCH --partition=debug
#SBATCH --nodes=1
# number of tasks (processes) per node
#SBATCH --ntasks-per-node=1
# number of cpus (threads) per task (process)
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=all
#SBATCH --mail-user=srattan1@jhu.edu
#SBATCH --output=hello-job.out

#### load and unload modules you may need
# module unload openmpi/intel
# module load mvapich2/gcc/64/2.0b
module load matlab
module list

#### execute code and write output file to OUT-24log.
# time mpiexec ./code-mvapich.x > OUT-24log
./hello.sh
echo "Finished with job $SLURM_JOBID"

#### mpiexec by default launches number of tasks requested
